# Written by dot arc (@moiusrname)
# Bing Scrapper Source: https://github.com/gurugaurav/bing_image_downloader

"""
✘ **Get Image from Bing !!**
• `{i}bing Monsoon`
• `{i}bing -gif Snowfall ; <count>`
• `{i}bing -line Avengers ; 10`

✘ Available filters:
•  `-gif` - Get GIFs
•  `-nsfw` - show nsfw content.
•  `-line`
•  `-transparent`
•  `-clipart`
"""

import asyncio
from pathlib import Path
import re

from pyUltroid.custom.bing_image import BingScrapper

from . import (
    LOGS,
    async_searcher,
    get_string,
    osremove,
    split_list,
    ultroid_cmd,
)


@ultroid_cmd(pattern="bing( (.*)|$)", manager=True)
async def bing_image(e):
    query = e.pattern_match.group(2)
    limit = 8
    if not query and e.reply_to:
        reply = await e.get_reply_message()
        if reply.text:
            query = reply.message
    if not query:
        return await e.eor("`give some text as well..`", time=5)

    msg = await e.eor(get_string("com_1"))
    img_type = "photo"
    arguments = ("doc", "gif", "nsfw", "clipart", "transparent", "line")
    args_dict = {i: False for i in arguments}
    for arg in arguments:
        pattern = re.compile(f"(?i)^ ?-{arg}")
        if re.match(pattern, query):
            re_sub = re.sub(pattern, "", query)
            args_dict[arg] = True
            query = re_sub.lstrip()

    as_doc, nsfw = args_dict.get("doc"), args_dict.get("nsfw")
    for _filter in ("clipart", "transparent", "line", "gif"):
        if args_dict.get(_filter):
            img_type = _filter
    del arguments, args_dict

    if ";" in query:
        query, limit = map(lambda i: i.strip(), query.split(";", maxsplit=1))
        limit = int(limit) if limit.isdigit() else 8

    bing_scp = BingScrapper(
        query=query,
        limit=limit,
        filter=img_type,
        hide_nsfw=not nsfw,
    )
    try:
        out_dir = await bing_scp.download()
        files = list(Path(out_dir).iterdir())
        if not files:
            return await msg.edit("`No files found on Server..`")
    except Exception as exc:
        LOGS.exception(exc)
        return await msg.edit(f"Error while Downloading Media: `{exc}`")

    total_files = len(files)
    # Sending gifs in album gives error and hence this exists
    photo_path, gif_path = [], []
    for path in files:
        _path = gif_path if path.suffix.lower() in (".gif", ".mp4") else photo_path
        _path.append(str(path))
    files = split_list(photo_path, 8) + gif_path
    del photo_path, gif_path

    await msg.edit(f"`Downloaded {total_files} files,\nUploading now..`")
    upload_count = 0
    reply_to = e.reply_to_msg_id or e.id
    for path in files:
        if type(path) == list:
            upload_count += len(path)
            caption = list(map(lambda _: "", path))
            caption[-1] = f"__**{query}** - ({upload_count}/{total_files})__"
        else:
            upload_count += 1
            caption = f"__**{query}** - ({upload_count}/{total_files})__"
        try:
            await e.client.send_file(
                e.chat_id,
                file=path,
                caption=caption,
                reply_to=reply_to,
                silent=e.reply_to,
                force_document=as_doc,
            )
        except Exception as exc:
            LOGS.exception(exc)
            continue
        finally:
            await asyncio.sleep(6)

    osremove(out_dir, folders=True)
    await msg.edit(f"`Uploaded {img_type}s from Bing!`")
