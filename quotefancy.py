# Ultroid - UserBot
# Copyright (C) 2020 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

"""
✘ Commands Available

• `{i}qfancy`
    Gets random quotes from QuoteFancy.com.
"""

from quotefancy import get_quote
from telethon.errors import ChatSendMediaForbiddenError

from . import run_async, ultroid_cmd, get_string, osremove


@run_async
def generate(_type):
    if _type == "img":
        quote = get_quote(type=_type, download=True)
    elif _type == "text":
        quote = get_quote(type=_type)
    return quote


@ultroid_cmd(pattern="qfancy( -t)?$")
async def quotefancy(e):
    mes = await e.eor(get_string("com_1"))
    if bool(e.pattern_match.group(1)):
        quote = await generate("text")
        return await mes.edit(f"`{quote}`")

    img = await generate("img")
    try:
        await e.client.send_file(e.chat_id, img, reply_to=e.reply_to_msg_id)
        osremove(img)
        await mes.delete()
    except ChatSendMediaForbiddenError:
        quote = await generate("text")
        await mes.edit(f"`{quote}`")
    except Exception as err:
        await mes.edit(f"**ERROR** - `{err}`")
