# Ultroid - UserBot
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.
#
# tts- Ported from Telebot

"""
✘ Commands Available -

✘ **Convert Text into Voice.**
• `{i}tts` `LanguageCode | <ur text/reply>`
    eg: `{i}tts en | Hello there!`

✘ **Convert Speech to Text.**
• `{i}stt` `<reply to audio/voice file>`
    eg: `{i}stt en` > as reply to an audio file.
    __Note - It is not 100% Accurate!__
"""

import os
import time
from shlex import quote

import speech_recognition
from gtts import gTTS

from . import (
    LOGS,
    bash,
    check_filename,
    get_string,
    mediainfo,
    run_async,
    time_formatter,
    ultroid_cmd,
)


@run_async
def _text_to_speech(text, language, path):
    tts = gTTS(text, lang=language)
    tts.save(path)


@run_async
def _speech_to_text(filepath, language):
    recogniser = speech_recognition.Recognizer()
    with speech_recognition.AudioFile(filepath) as source:
        audio = recogniser.record(source)
    return recogniser.recognize_google(audio, language=language)


@ultroid_cmd(
    pattern="tts( (.*)|$)",
)
async def text2speech(event):
    input_str = event.pattern_match.group(2)
    ult = await event.eor(get_string("com_1"))
    if event.is_reply:
        previous_message = await event.get_reply_message()
        text = previous_message.message
        lang = input_str or "en"
    elif input_str:
        if "|" in input_str:
            lang, text = input_str.split("|")
        else:
            lang, text = "en", input_str
    else:
        return await ult.edit("`Invalid Syntax. Module stopping.`")

    start = time.time()
    text, lang = text.strip(), lang.strip()
    required_file_name = check_filename("tts.ogg")
    await _text_to_speech(text=text, language=lang, path=required_file_name)
    opus_path = check_filename("tts.opus")
    _out, _err = await bash(
        f"ffmpeg -hide_banner -loglevel error -i {required_file_name} -map 0:a -codec:a libopus -b:a 64k -vbr on -application voip {opus_path} -y"
    )
    ms = time_formatter((time.time() - start) * 1000)
    if not os.path.exists(opus_path):
        LOGS.exception(_err)
        opus_path = required_file_name
    else:
        os.remove(required_file_name)

    caption = f"Converted to Voice ({lang}) in {ms}."
    try:
        await event.client.send_file(
            event.chat_id,
            file=opus_path,
            caption=caption,
            voice_note=True,
            silent=event.is_reply,
            reply_to=event.reply_to_msg_id or event.id,
        )
        await ult.delete()
    except Exception as e:
        await ult.edit(f"**Error:** `{e}`")
    finally:
        os.remove(opus_path)


@ultroid_cmd(pattern="stt( (.*)|$)")
async def speech2text(e):
    reply = await e.get_reply_message()
    if not (reply and reply.media and "audio" in mediainfo(reply.media)):
        return await e.eor("`Reply to Audio-File..`", time=6)

    lang = e.pattern_match.group(2) or "en-IN"
    ult = await e.eor(get_string("com_1"))
    path = await reply.download_media()
    out = check_filename(os.path.splitext(path)[0] + ".wav")
    await bash(
        f"ffmpeg -hide_banner -loglevel error -i {quote(path)} -vn {quote(out)} -y"
    )
    try:
        text = await _speech_to_text(filepath=out, language=lang)
        await ult.edit(f"**Extracted Text ({lang}) -** \n\n`{text}`")
    except Exception as er:
        LOGS.exception(er)
        await ult.edit(f"**Error:** `{er}`")
    finally:
        for i in (out, path):
            if os.path.exists(i):
                os.remove(i)
