# Ultroid - UserBot
# Copyright (C) 2020 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

"""
✘ Commands Available -

• `{i}truth`
   `Get Truth Task.`

• `{i}dare`
   `Get Dare Task.`
"""

from bs4 import BeautifulSoup

from . import ultroid_cmd, async_searcher


API = "https://fungenerators.com/random/truth-or-dare?option="


@ultroid_cmd(pattern="truth$")
async def truth_n_dare(ult):
    m = await ult.eor("`Generating a Truth Statement.. `")
    link = API + "truth"
    resp = await async_searcher(link, re_content=True)
    parser = BeautifulSoup(resp, "html.parser")
    output = parser.find_all("h2")[0].text
    await m.edit(f"**#TruthTask**\n\n`{output}`")


@ultroid_cmd(pattern="dare$")
async def gtruth(ult):
    m = await ult.eor("`Generating a Dare Task.. `")
    link = API + "dare"
    res = await async_searcher(link, re_content=True)
    bsc = BeautifulSoup(ct, "html.parser")
    output = bsc.find_all("h2")[0].text
    await m.edit(f"**#DareTask**\n\n`{output}`")
