# < Made for Ultroid by @Spemgod! >
# < https://github.com/TeamUltroid/Ultroid >

"""
✘ **Download Forward restricted files!**

• **Example:**
>  `{i}fwdl <msg_link>`
>  `{i}fwdl -m https://t.me/nofwd/14 | https://t.me/nofwd/18`
"""

import asyncio
from re import findall

from telethon.errors.rpcerrorlist import (
    ChatForwardsRestrictedError,
    MessageNotModifiedError,
)

from pyUltroid.custom._transfer import pyroUL
from . import asst, check_filename, eod, LOGS, random_string, ultroid_cmd, unix_parser


def get_chat_and_msg_id(link):
    # Source: https://github.com/UsergeTeam/Userge/blob/7eef3d2bec25caa53e88144522101819cb6cb649/userge/plugins/misc/download.py#L76
    TG_LINK_REGEX = r"^(?:(?:https?|tg):\/\/)?(?:www\.)?(?:t\.me\/|openmessage\?)(?:(?:c\/(\d+))|(\w+)|(?:user_id\=(\d+)))(?:\/|&message_id\=)(\d+)\/?(\d+)?(?:\?single)?$"

    if remgx := findall(TG_LINK_REGEX, link.strip()):
        # chat_id / topic_id / msg_id
        ids = tuple(filter(bool, remgx[0]))
        return ids[0], int(ids[-1])


def _parse(id):
    try:
        return int(id)
    except ValueError:
        return id


@ultroid_cmd(
    pattern="fw(dl|)(?: |$)(.*)",
    fullsudo=True,
)
async def fwd_dlx(e):
    single = False
    _args = e.pattern_match.group(2)
    flags = unix_parser(_args or "")
    args = flags.args
    ghomst = await e.eor("...")
    if not args:
        reply = await e.get_reply_message()
        if reply and reply.text:
            flags = unix_parser(reply.text)
            args = flags.args
        else:
            return await eod(ghomst, "Give a tg link to download", time=10)

    if "|" in args:
        spl = [i.strip() for i in args.split("|", maxsplit=1)]
        arg1 = get_chat_and_msg_id(spl[0])
        arg2 = get_chat_and_msg_id(spl[1])
    else:
        single = True
        arg1 = get_chat_and_msg_id(args)

    if not arg1:
        return await ghomst.edit("`probably a invalid Link !?`")

    media_only = "m" in flags.kwargs
    chat = _parse(arg1[0])
    await ultroid.get_entity(await e.client.parse_id(chat))

    if single:
        n = arg1[1]
        min_id, max_id = n - 1, n + 1
    else:
        min_id, max_id = arg1[1] - 1, arg2[1] + 1
        if arg1[0] != arg2[0] or min_id > max_id:
            return await ghomst.edit("Check Order of links.")

    async for x in ultroid.iter_messages(
        chat,
        reverse=True,
        min_id=min_id,
        max_id=max_id,
    ):
        if x.action:
            continue
        elif media_only and not x.media:
            continue
        if ghomst.client._bot and x.chat.username:
            x = await asst.get_messages(x.chat.username, ids=x.id)
        await copy_msg(x, ghomst)
        await asyncio.sleep(3.5)
    await ghomst.try_delete()


async def copy_msg(msg, mymsg):
    try:
        return await msg.copy(mymsg.chat_id, reply_to=mymsg.id)
    except ChatForwardsRestrictedError:
        pass

    if not msg.media:
        await mymsg.client.send_message(mymsg.chat_id, msg.text)
        return

    if doc := getattr(msg, "document", 0):
        filename = msg.file.name or msg.message_link
        media = await mymsg.client.fast_downloader(
            doc,
            show_progress=True,
            event=mymsg,
            message=f"Downloading {filename}...",
        )
        media = media[0].name
    else:
        await mymsg.edit(f"`Downloading {msg.message_link}...`")
        media = await msg.download_media("resources/downloads/")

    try:
        await asyncio.sleep(1)
        await mymsg.edit("`Uploading ...`")
    except MessageNotModifiedError:
        pass

    x = pyroUL(event=mymsg, _path=media)
    await x.upload(
        _log=False,
        caption=msg.text,
        reply_to=mymsg.id,
        df=True,
        auto_edit=False,
        delete_file=True,
    )
