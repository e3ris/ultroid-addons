# Ultroid - UserBot
# Copyright (C) 2020-2023 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://github.com/TeamUltroid/pyUltroid/blob/main/LICENSE>.

import asyncio
import json
import os
import time
from mimetypes import guess_type
from urllib.parse import parse_qs, urlencode

import aiofiles
from aiohttp import ClientSession
from aiohttp.client_exceptions import ContentTypeError
from telethon.tl.types import Message

from . import (
    LOGS,
    check_filename,
    get_string,
    humanbytes,
    random_string,
    tg_downloader,
    time_formatter,
    udB,
    ultroid_cmd,
)


class GDrive:
    __slots__ = (
        "base_url",
        "client_id",
        "client_secret",
        "folder_id",
        "scope",
        "creds",
    )

    def __init__(self):
        self.base_url = "https://www.googleapis.com/drive/v3"
        self.client_id = udB.get_key("GDRIVE_CLIENT_ID")
        self.client_secret = udB.get_key("GDRIVE_CLIENT_SECRET")
        self.folder_id = udB.get_key("GDRIVE_FOLDER_ID") or "root"
        self.scope = "https://www.googleapis.com/auth/drive"
        self.creds = udB.get_key("GDRIVE_AUTH_TOKEN") or {}

    def get_oauth2_url(self):
        return "https://accounts.google.com/o/oauth2/v2/auth?" + urlencode(
            {
                "client_id": self.client_id,
                "redirect_uri": "http://localhost",
                "response_type": "code",
                "scope": self.scope,
                "access_type": "offline",
                "prompt": "consent",
            }
        )

    async def get_access_token(self, code=None) -> dict:
        if code.startswith("http://localhost"):
            # get all url arguments
            code = parse_qs(code.split("?")[1]).get("code")[0]
        url = "https://oauth2.googleapis.com/token"
        async with ClientSession() as client:
            resp = await client.post(
                url,
                data={
                    "client_id": self.client_id,
                    "client_secret": self.client_secret,
                    "redirect_uri": "http://localhost",
                    "grant_type": "authorization_code",
                    "code": code,
                },
                headers={"Content-Type": "application/x-www-form-urlencoded"},
            )
            self.creds = await resp.json()
        self.creds["expires_in"] = time.time() + 3590
        udB.set_key("GDRIVE_AUTH_TOKEN", self.creds)
        return self.creds

    async def refresh_access_token(self) -> None:
        async with ClientSession() as client:
            resp = await client.post(
                "https://oauth2.googleapis.com/token",
                data={
                    "client_id": self.client_id,
                    "client_secret": self.client_secret,
                    "grant_type": "refresh_token",
                    "refresh_token": self.creds.get("refresh_token"),
                },
                headers={"Content-Type": "application/x-www-form-urlencoded"},
            )
            self.creds["access_token"] = (await resp.json())["access_token"]
        self.creds["expires_in"] = time.time() + 3590
        udB.set_key("GDRIVE_AUTH_TOKEN", self.creds)

    async def get_size_status(self) -> dict:
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        async with ClientSession() as client:
            resp = await client.get(
                self.base_url + "about",
                headers={
                    "Authorization": "Bearer " + self.creds.get("access_token"),
                    "Content-Type": "application/json",
                },
                params={"fields": "storageQuota"},
            )
            return await resp.json()

    async def set_permissions(
        self, fileid: str, role: str = "reader", type: str = "anyone"
    ):
        # set permissions to anyone with link can view
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        async with ClientSession() as client:
            resp = await client.post(
                self.base_url + f"/files/{fileid}/permissions",
                headers={
                    "Authorization": "Bearer " + self.creds.get("access_token"),
                    "Content-Type": "application/json",
                },
                json={
                    "role": role,
                    "type": type,
                },
            )
            return await resp.json()

    async def list_files(self) -> dict:
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        async with ClientSession() as client:
            resp = await client.get(
                self.base_url + "/files",
                headers={
                    "Authorization": "Bearer " + self.creds.get("access_token"),
                    "Content-Type": "application/json",
                },
            )
            return await resp.json()

    async def delete(self, fileId: str) -> dict:
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        async with ClientSession() as client:
            r = await client.delete(
                self.base_url + f"/files/{fileId}",
                headers={
                    "Authorization": "Bearer " + self.creds.get("access_token"),
                    "Content-Type": "application/json",
                },
            )
            try:
                return await r.json()
            except ContentTypeError:
                return {"status": "success"}

    async def copy_file(
        self, fileId: str, filename: str, folder_id: str, move: bool = False
    ):
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        headers = {
            "Authorization": "Bearer " + self.creds.get("access_token"),
            "Content-Type": "application/json",
        }
        params = {
            "name": filename,
            "mimeType": "application/octet-stream",
            "fields": "id, name, webContentLink",
            "supportsAllDrives": "true",
        }
        file_metadata = {
            "name": filename,
            "fileId": fileId,
        }
        if folder_id:
            file_metadata["parents"] = [folder_id]
        elif self.folder_id:
            file_metadata["parents"] = [self.folder_id]
        params["addParents"] = folder_id if folder_id else self.folder_id
        params["removeParents"] = "root" if move else None
        async with ClientSession() as client:
            resp = await client.patch(
                self.base_url + f"/files/{fileId}",
                headers=headers,
                data=json.dumps(file_metadata),
                params=params,
            )
            r = await resp.json()
            if r.get("error") and r["error"]["code"] == 401:
                await self.refresh_access_token()
                return await self.copy_file(fileId, filename, folder_id, move)
            return r

    async def upload_file(self, event, path: str, folder_id: str = None):
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        mime_type = guess_type(path)[0] or "application/octet-stream"
        # upload with progress bar
        filesize = os.path.getsize(path)
        filename = os.path.basename(path)
        # await self.get_size_status()
        chunksize = 50 * 1024 * 1024  # default = 104857600  # 100MB
        # 1. Retrieve session for resumable upload.
        headers = {
            "Authorization": "Bearer " + self.creds.get("access_token"),
            "Content-Type": "application/json",
        }
        params = {
            "name": filename,
            "mimeType": mime_type,
            "fields": "id, name, webContentLink",
            "parents": [folder_id] if folder_id else [self.folder_id],
        }
        async with ClientSession() as client:
            r = await client.post(
                "https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable",
                headers=headers,
                data=json.dumps(params),
                params={"fields": "id, name, webContentLink"},
            )
            if r.status == 401:
                await self.refresh_access_token()
                return await self.upload_file(event, path, folder_id)
            elif r.status == 403:
                # upload to root and move
                r = await self.upload_file(event, path, "root")
                return await self.copy_file(r["id"], filename, folder_id, move=True)
            upload_url = r.headers.get("Location")

        uploaded = 0
        trigger_count = 0
        resp = None
        last_txt = ""
        start = time.time()
        async with aiofiles.open(path, mode="rb") as f:
            while filesize != uploaded:
                chunk_data = await f.read(chunksize)
                headers = {
                    "Content-Length": str(len(chunk_data)),
                    "Content-Range": f"bytes {uploaded}-{uploaded + len(chunk_data) - 1}/{filesize}",
                }
                uploaded += len(chunk_data)
                async with ClientSession() as client:
                    resp = await client.put(
                        upload_url,
                        data=chunk_data,
                        headers=headers,
                    )
                trigger_count += 1
                if trigger_count % 4 != 0:
                    continue
                diff = time.time() - start
                percentage = round((uploaded / filesize) * 100, 2)
                speed = round(uploaded / diff, 2)
                eta = round((filesize - uploaded) / speed, 2) * 1000
                crnt_txt = (
                    f"Uploading `{filename}` to **GDrive**...\n\n"
                    + f"**Status:**  `{humanbytes(uploaded)}/{humanbytes(filesize)}` » `{percentage}%`\n"
                    + f"**Speed:**  `{humanbytes(speed)}/s`\n"
                    + f"**ETA:**  `{time_formatter(eta)}`"
                )
                if last_txt != crnt_txt:
                    await event.edit(crnt_txt)
                    last_txt = crnt_txt
            return await resp.json()

    @staticmethod
    def extract_GD_ids(link):
        if "uc?id=" in link:
            # https://drive.google.com/uc?id=1c7dE6hiYnTKlyBnWV4HrdGMkAhun_FZY&export=download
            spl = link.split("id=", maxsplit=1)[1]
            return spl.split("&", maxsplit=1)[0]
        elif "file/d/" in link:
            # https://drive.google.com/file/d/1mFKVR1_eNOf279TD_KrAvkHOKPiOcW2Y/view?usp=drive_link
            spl = link.split("file/d/", maxsplit=1)[1]
            return spl.split("/view", maxsplit=1)[0]

    async def download_file(
        self,
        event,
        fileId: str,
    ):
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        fileId = self.extract_GD_ids(fileId)
        last_txt = ""
        chunksize = 60 * 1024 * 1024  # 60MB
        headers = {
            "Authorization": "Bearer " + self.creds.get("access_token"),
            "Content-Type": "application/json",
        }
        params = {
            "supportsAllDrives": "true",
            "includeItemsFromAllDrives": "true",
            "fields": "id, name, mimeType, size",
            "parents": [self.folder_id],
        }
        async with ClientSession() as client:
            r = await client.get(
                self.base_url + f"/files/{fileId}",
                headers=headers,
                params=params,
            )
            if r.status != 200:
                try:
                    js = await r.json()
                except Exception:
                    js = await r.text()
                return False, js

            resp = await r.json()

        filename = resp.get("name", random_string(16, numbers=True))
        filename = check_filename(f"resources/downloads/{filename}")
        filesize = int(resp.get("size", 1))
        downloaded = 0
        trigger_count = 0
        start = time.time()
        async with aiofiles.open(filename, "wb") as f:
            async with ClientSession() as client:
                resp1 = await client.get(
                    self.base_url + f"/files/{fileId}",
                    headers=headers,
                    params={"alt": "media", **params},
                    timeout=None,
                )
                async for chunk in resp1.content.iter_chunked(chunksize):
                    downloaded += await f.write(chunk)
                    trigger_count += 1
                    if trigger_count % 5 != 0:
                        continue
                    diff = time.time() - start
                    percentage = round((downloaded / filesize) * 100, 2)
                    speed = round(downloaded / diff, 2)
                    eta = round((filesize - downloaded) / speed, 2) * 1000
                    crnt_txt = (
                        f"Downloading `{filename}` from GDrive...\n\n"
                        + f"**Status:**  `{humanbytes(downloaded)}/{humanbytes(filesize)}` » `{percentage}%`\n"
                        + f"**Speed:**  `{humanbytes(speed)}/s`\n"
                        + f"**ETA:**  `{time_formatter(eta)}`"
                    )
                    if last_txt != crnt_txt:
                        await event.edit(crnt_txt)
                        trigger_count = 0
                        last_txt = crnt_txt

        return True, filename


@ultroid_cmd(
    pattern="gup2( (.*)|$)",
    fullsudo=True,
)
async def gup2(event):
    GD = GDrive()
    if not GD.creds:
        return await event.eor(
            "`Credentials have not been added!... \n\nAdd GDRIVE_TOKEN to Use it..`"
        )
    input_file = event.pattern_match.group(2) or await event.get_reply_message()
    if not input_file:
        return await event.eor(
            "`Reply to file or give its location to upload to Gdrive!`"
        )

    mone = await event.eor(get_string("com_1"))
    if isinstance(input_file, Message):
        try:
            filename, t_time = await tg_downloader(
                media=input_file,
                event=mone,
                show_progress=True,
            )
        except Exception as exc:
            LOGS.exception(exc)
            return await mone.edit(f"**Error in downloading file:** `{exc}`")
        tt = time_formatter(t_time * 1000)
        await mone.edit(f"Downloaded to \n`{filename}`\n in {tt}..")
        await asyncio.sleep(2)
    else:
        filename = input_file

    if not (os.path.isfile(filename) and os.path.getsize(filename) != 0):
        return await mone.eor(
            "`File Not found in local server or File Size is 0B.\n\n Give me a valid file path :((`",
            time=6,
        )

    try:
        m_time = time.time()
        resp = await GD.upload_file(mone, filename)
        await asyncio.sleep(1.5)
        text = "**GDrive Upload was Successful!** \n\n**File Name:**  `{name}` \n**Size:**  `{size}` \n**GDrive Link:**  [Click Here]({link}) \n**Time Taken:**  `{time_taken}`"
        await mone.edit(
            text.format(
                name=resp.get("name"),
                size=humanbytes(os.path.getsize(filename)),
                link=resp.get("webContentLink"),
                time_taken=time_formatter((time.time() - m_time) * 1000),
            )
        )
    except Exception as exc:
        LOGS.exception(exc)
        await mone.edit(f"Exception occurred while uploading to gDrive: \n`{exc}`")


@ultroid_cmd(
    pattern="gdown2( (.*)|$)",
)
async def gdown2(e):
    GD = GDrive()
    match = e.pattern_match.group(2)
    if not match:
        return await e.eor("`Give GDrive Link to Download from!`")

    file_id = GD.extract_GD_ids(match)
    if not file_id:
        return await e.eor("`Invalid GDrive Link given!?..?`")

    eve = await e.eor(get_string("com_1"))
    _start = time.time()
    status, response = await GD.download_file(eve, match)
    if not status:
        LOGS.exception(response)
        return await eve.edit(f"`Exception occurred while Downloading from gDrive...`")

    source = f"[GDrive]({match})" if "https" in match else "GDrive"
    size = humanbytes(os.path.getsize(response))
    time_taken = time_formatter((time.time() - _start) * 1000)
    text = f"**GDrive Download was Successful!** \n\n**Path:**  `{response}` \n**Size:**  `{size}` \n**Source:**  {source} \n**Time Taken:**  `{time_taken}`"
    await eve.edit(text)
