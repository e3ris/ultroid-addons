# Ultroid - UserBot
# Copyright (C) 2021-2022 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

"""
✘ Commands Available -

• `{i}ncode <file>`
   Use: Paste the contents of file and send as pic.
"""

import pygments
from pygments.formatters import ImageFormatter
from pygments.lexers import Python3Lexer

from . import asyncread, LOGS, ultroid_cmd, check_filename, mediainfo, osremove


@ultroid_cmd(pattern="ncode$")
async def code_print(event):
    my = await event.eor("`Processing...`")
    if not event.is_reply:
        return await my.edit("`Reply to a file or message!`", time=5)
    reply = await event.get_reply_message()
    if reply.media and mediainfo(reply.media) == "document":
        path = await event.client.download_media(reply)
        try:
            txt = await asyncread(path)
        except Exception as exc:
            LOGS.exception(exc)
            return await my.edit("`Invalid File...`")
        finally:
            osremove(path)
    elif reply.text:
        txt = reply.message

    filepath = check_filename("ncode_output.png")
    try:
        pygments.highlight(
            txt,
            Python3Lexer(),
            ImageFormatter(line_numbers=True),
            filepath,
        )
        await event.client.send_file(
            event.chat_id,
            filepath,
            force_document=True,
            caption=f"**{filepath}**",
            reply_to=reply.id,
        )
        await my.delete()
    except Exception as exc:
        LOGS.exception(exc)
        await my.edit(f"**Error:**  `{exc}`")
    finally:
        osremove(filepath)
