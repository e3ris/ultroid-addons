# Ultroid - UserBot
# Copyright (C) 2020 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

"""
✘ Commands Available

• `{i}joke`
    To get joke.

• `{i}url <long url>`
    To get a shorten link of long link.

• `{i}phlogo <first_name> <last_name>`
    Make a phub based logo.

• `{i}decide`
    Decide something.

• `{i}xo`
    Opens tic tac game only where using inline mode is allowed.

• `{i}wordi`
    Opens word game only where using inline mode is allowed.

• `{i}gps <name of place>`
    Shows the desired place in the map.
"""

import os
import random
from time import time

from pyjokes import get_joke
from telethon.errors import ChatSendMediaForbiddenError

from . import HNDLR, ultroid_cmd, get_string, async_searcher, osremove

try:
    from PIL import Image, ImageDraw, ImageFont
except ImportError:
    Image = None


@ultroid_cmd(pattern="joke$")
async def get_jomke(ult):
    await ult.eor(get_joke())


@ultroid_cmd(pattern="url( (.*)|$)")
async def shorty_url(event):
    input_str = event.pattern_match.group(2)
    if not input_str:
        return await event.eor("`Give some url..`")
    sample_url = "https://da.gd/s?url={}".format(input_str)
    response_api = await async_searcher(sample_url)
    out = (
        f"**Shortened url** ==> {response_api}\n**Given url** ==> {input_str}"
        if response_api
        else "`Something went wrong. Please try again Later..`"
    )
    await event.eor(out)


@ultroid_cmd(pattern="decide$")
async def yes_no(event):
    hm = await event.eor("`Deciding..`")
    r = await async_searcher("https://yesno.wtf/api", re_json=True)
    try:
        await event.reply(r["answer"], file=r["image"])
        await hm.delete()
    except ChatSendMediaForbiddenError:
        await event.eor(r["answer"])


@ultroid_cmd(pattern="xo$")
async def xo(ult):
    xox = await ult.client.inline_query("xobot", "play")
    await xox[random.randrange(0, len(xox) - 1)].click(
        ult.chat_id, reply_to=ult.reply_to_msg_id, silent=True, hide_via=True
    )
    await ult.delete()


def _add_corners(img, radius):
    circle = Image.new("L", (radius * 2, radius * 2), 0)
    draw = ImageDraw.Draw(circle)
    draw.ellipse((0, 0, radius * 2, radius * 2), fill=255)
    alpha = Image.new("L", img.size, 255)
    w, h = img.size
    alpha.paste(circle.crop((0, 0, radius, radius)), (0, 0))
    alpha.paste(circle.crop((0, radius, radius, radius * 2)), (0, h - radius))
    alpha.paste(circle.crop((radius, 0, radius * 2, radius)), (w - radius, 0))
    alpha.paste(
        circle.crop((radius, radius, radius * 2, radius * 2)), (w - radius, h - radius)
    )
    img.putalpha(alpha)
    return img


def _gabung(font, img_obj, text1):
    img = _add_corners(img_obj, 17)
    new_img = Image.new("RGB", (40, 20), color=(0, 0, 0))
    draw = ImageDraw.Draw(new_img)
    size = round(font.getlength(text1))
    baru = Image.new("RGB", (img.width + size + 210 + 20 + 130, 600), color=(0, 0, 0))
    draw = ImageDraw.Draw(baru)
    draw.text((150, 250), text1, (255, 255, 255), font=font)
    baru.paste(img, (150 + size + 20, 230 + 10), img.convert("RGBA"))
    return baru


# https://github.com/krypton-byte/drawHub
def generate(font, text1, text2):
    text = font.getbbox(text2)
    length = round(font.getlength(text2))
    height = (round(text[3]) + 10) - round(text[1])
    oren = Image.new("RGBA", (length + 20, 140), color=(240, 152, 0))
    draw = ImageDraw.Draw(oren)
    draw.text((10, int((oren.height - height) / 2) - 10), text2, (0, 0, 0), font=font)
    out = _gabung(font, oren, text1)
    name = f"{time()}.png"
    out.save(name, "PNG")
    return name


@ultroid_cmd(pattern="phlogo( (.*)|$)")
async def make_phlogo(ult):
    if not Image:
        return await ult.eor("Install `pillow` to use this Command!", time=10)

    msg = await ult.eor(get_string("com_1"))
    match = ult.pattern_match.group(2)
    reply = await ult.get_reply_message()
    if not match and (reply and reply.text):
        match = reply.message
    if not match:
        return await msg.edit(f"`Provide a name to make phlogo...`")

    first, last = "", ""
    if " " in match:
        first, last = match.split(maxsplit=1)
    else:
        last = match
    font = ImageFont.truetype(
        "resources/fonts/expressway.ttf"
        if os.path.isfile("resources/fonts/expressway.ttf")
        else "resources/fonts/Roboto-Regular.ttf",
        110,
    )
    logo = generate(font, first, last)
    await ult.client.send_message(
        ult.chat_id, file=logo, reply_to=ult.reply_to_msg_id or ult.id
    )
    osremove(logo)
    await msg.delete()


@ultroid_cmd(pattern="(gps|wordi) (.*)")
async def _in_game(ult):
    cmd = ult.pattern_match.group(1)
    get = ult.pattern_match.group(2)
    if not get:
        return await ult.eor(f"Use this command as `{HNDLR}{cmd} <query>`")
    Bot = {"gps": "openmap_bot", "wordi": "wordibot"}
    quer = await ult.client.inline_query(Bot[cmd], get)
    await quer[0].click(
        ult.chat_id, reply_to=ult.reply_to_msg_id, silent=True, hide_via=True
    )
    await ult.delete()
