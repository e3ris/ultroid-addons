# Ported From DarkCobra Originally By UNIBORG
#
# Ultroid - UserBot
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

"""
✘ Commands Available -

• `{i}test`
    Test your Server Speed. (ookla speedtest package)

•  `{i}temst`
   Test your Server Speed. (speedtest-cli)
"""

from time import time
from json import loads

from . import bash, humanbytes, run_async, ultroid_cmd


@run_async
def __speedtest(speedtest):
    s = speedtest.Speedtest()
    s.get_best_server()
    s.download()
    s.upload()
    return s.results.dict(), s.results.share()


@ultroid_cmd(pattern="(speed)?test( (.*)|$)")
async def speedtest_ookla(event):
    try:
        import speedtest
    except ImportError:
        return await event.eor("Install 'speedtest-cli' to use this plugin!")

    input_str = event.pattern_match.group(3)
    has_speed = bool(event.pattern_match.group(1))
    as_image = has_speed or input_str in ("image", "img", "pic", "photo")
    xx = await event.eor("`Calculating ur Ultroid Server Speed. Please wait!`")

    try:
        start = time()
        response, speedtest_image = await __speedtest(speedtest)
        ms = f"{time() - start:.2f}"
    except Exception as exc:
        return await xx.edit(f"**Error while performing Speedtest:** \n\n`{exc}`")

    download_speed = response.get("download")
    upload_speed = response.get("upload")
    ping_time = response.get("ping")
    client_infos = response.get("client")
    i_s_p = client_infos.get("isp")
    i_s_p_rating = client_infos.get("isprating")

    try:
        if not as_image:
            await xx.edit(
                """`Ultroid Server Speed in {} sec`

`Download: {}`
`Upload: {}`
`Ping: {}`
`Internet Service Provider: {}`
`ISP Rating: {}`""".format(
                    ms,
                    humanbytes(download_speed),
                    humanbytes(upload_speed),
                    ping_time,
                    i_s_p,
                    i_s_p_rating,
                )
            )
        else:
            caption = f"<b><a href={speedtest_image}>SpeedTest</a> completed in {ms} second.</b>"
            await event.client.send_file(
                event.chat_id,
                speedtest_image,
                caption=caption,
                parse_mode="html",
                silent=not event.reply_to,
                reply_to=event.reply_to_msg_id if event.reply_to else event.id,
            )
            await xx.delete()
    except Exception as exc:  # dc
        await xx.edit(
            """**SpeedTest** completed in {} seconds
`Download: {}`
`Upload: {}`
`Ping: {}`


__With the Following ERRORs__
{}""".format(
                ms,
                humanbytes(download_speed),
                humanbytes(upload_speed),
                ping_time,
                str(exc),
            )
        )


@ultroid_cmd(pattern="temst$")
async def speemdtest(event):
    xx = await event.eor("`Calculating Server Speed...`")
    start = time()
    out, err = await bash("speedtest -f json-pretty")
    try:
        url = loads(out)["result"]["url"] + ".png"
    except Exception as exc:
        return await xx.edit(f"**Error:** \n`{exc}`")

    time_taken = f"{time() - start:.2f}"
    await event.client.send_file(
        event.chat_id,
        url,
        caption=f"<b><a href={url}>SpeedTest</a> completed in {time_taken} second.</b>",
        reply_to=event.reply_to_msg_id or event.id,
        parse_mode="html",
    )
    await xx.delete()
