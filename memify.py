# Ported Nd Modified For Ultroid
# Ported From DarkCobra (Modified by @ProgrammingError)
#
# Ultroid - UserBot
# Copyright (C) 2020 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

"""
✘ Commands Available -

• `{i}mmf <upper text> ; <lower text> <reply to media>`
    To create memes as sticker,
    for trying different fonts use (.mmf <text>_1)(u can use 1 to 10).

• `{i}mms <upper text> ; <lower text> <reply to media>`
    To create memes as pic,
    for trying different fonts use (.mms <text>_1)(u can use 1 to 10).
"""

import textwrap
from shlex import quote

import cv2
from PIL import Image, ImageDraw, ImageFont

from . import bash, ultroid_cmd, run_async, check_filename, osremove


@ultroid_cmd(pattern="mmf( (.*)|$)")
async def _mmf(event):
    ureply = await event.get_reply_message()
    msg = event.pattern_match.group(2)
    if not (ureply and (ureply.media)):
        return await event.eor("`Reply to any media`")
    if not msg:
        return await event.eor("`Give me something text to write...`")

    xx = await event.eor("`Processing`")
    ultt = await ureply.download_media()
    file = check_filename("ult.png")
    if ultt.endswith(".tgs"):
        await xx.edit("`Ooo Animated Sticker 👀...`")
        await bash(f"lottie_convert.py {quote(ultt)} {quote(file)}")
    elif ultt.endswith((".webp", ".png")):
        im = Image.open(ultt)
        im.save(file, format="PNG", optimize=True)
    else:
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite(file, lol)
    stick = await draw_meme_text(file, msg)
    await event.client.send_file(
        event.chat_id, stick, force_document=False, reply_to=event.reply_to_msg_id
    )
    osremove(ultt, stick)
    await xx.delete()


@run_async
def draw_meme_text(image_path, msg):
    img = Image.open(image_path)
    osremove(image_path)
    i_width, i_height = img.size
    if "_" in msg:
        text, font = msg.split("_")
    else:
        text = msg
        font = "default"
    if ";" in text:
        upper_text, lower_text = text.split(";")
    else:
        upper_text = text
        lower_text = ""
    draw = ImageDraw.Draw(img)
    m_font = ImageFont.truetype(
        f"resources/fonts/{font}.ttf", int((70 / 640) * i_width)
    )
    current_h, pad = 10, 5
    if upper_text:
        for u_text in textwrap.wrap(upper_text, width=15):
            dim = draw.textbbox((0, 0), u_text, font=m_font)
            u_width, u_height = dim[2] - dim[0], dim[3] - dim[1]
            draw.text(
                xy=(((i_width - u_width) / 2) - 1, int((current_h / 640) * i_width)),
                text=u_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(((i_width - u_width) / 2) + 1, int((current_h / 640) * i_width)),
                text=u_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=((i_width - u_width) / 2, int(((current_h / 640) * i_width)) - 1),
                text=u_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(((i_width - u_width) / 2), int(((current_h / 640) * i_width)) + 1),
                text=u_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=((i_width - u_width) / 2, int((current_h / 640) * i_width)),
                text=u_text,
                font=m_font,
                fill=(255, 255, 255),
            )
            current_h += u_height + pad
    if lower_text:
        for l_text in textwrap.wrap(lower_text, width=15):
            dim = draw.textbbox((0, 0), l_text, font=m_font)
            u_width, u_height = dim[2] - dim[0], dim[3] - dim[1]
            draw.text(
                xy=(
                    ((i_width - u_width) / 2) - 1,
                    i_height - u_height - int((80 / 640) * i_width),
                ),
                text=l_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(
                    ((i_width - u_width) / 2) + 1,
                    i_height - u_height - int((80 / 640) * i_width),
                ),
                text=l_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(
                    (i_width - u_width) / 2,
                    (i_height - u_height - int((80 / 640) * i_width)) - 1,
                ),
                text=l_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(
                    (i_width - u_width) / 2,
                    (i_height - u_height - int((80 / 640) * i_width)) + 1,
                ),
                text=l_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(
                    (i_width - u_width) / 2,
                    i_height - u_height - int((80 / 640) * i_width),
                ),
                text=l_text,
                font=m_font,
                fill=(255, 255, 255),
            )
            current_h += u_height + pad
    imag = check_filename("ult.webp")
    img.save(imag, "WebP")
    return imag


@ultroid_cmd(pattern="mms( (.*)|$)")
async def _mms(event):
    ureply = await event.get_reply_message()
    msg = event.pattern_match.group(2)
    if not (ureply and (ureply.media)):
        return await event.eor("`Reply to any media`")
    if not msg:
        return await event.eor("`Give me something text to write 😑`")

    xx = await event.eor("`Processing...`")
    ultt = await ureply.download_media()
    file = check_filename("ult.png")
    if ultt.endswith((".tgs")):
        await xx.edit("`Ooo Animated Sticker 👀...`")
        await bash(f"lottie_convert.py {quote(ultt)} {quote(file)}")
    elif ultt.endswith((".webp", ".png")):
        im = Image.open(ultt)
        im.save(file, format="PNG", optimize=True)
    else:
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite(file, lol)
    pic = await draw_meme(file, msg)
    await event.client.send_file(
        event.chat_id, pic, force_document=False, reply_to=event.reply_to_msg_id
    )
    osremove(ultt, pic)
    await xx.delete()


@run_async
def draw_meme(image_path, msg):
    img = Image.open(image_path)
    osremove(image_path)
    i_width, i_height = img.size
    if "_" in msg:
        text, font = msg.split("_")
    else:
        text = msg
        font = "default"
    if ";" in text:
        upper_text, lower_text = text.split(";")
    else:
        upper_text = text
        lower_text = ""
    draw = ImageDraw.Draw(img)
    m_font = ImageFont.truetype(
        f"resources/fonts/{font}.ttf", int((70 / 640) * i_width)
    )
    current_h, pad = 10, 5
    if upper_text:
        for u_text in textwrap.wrap(upper_text, width=15):
            dim = draw.textbbox((0, 0), u_text, font=m_font)
            u_width, u_height = dim[2] - dim[0], dim[3] - dim[1]
            draw.text(
                xy=(((i_width - u_width) / 2) - 1, int((current_h / 640) * i_width)),
                text=u_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(((i_width - u_width) / 2) + 1, int((current_h / 640) * i_width)),
                text=u_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=((i_width - u_width) / 2, int(((current_h / 640) * i_width)) - 1),
                text=u_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(((i_width - u_width) / 2), int(((current_h / 640) * i_width)) + 1),
                text=u_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=((i_width - u_width) / 2, int((current_h / 640) * i_width)),
                text=u_text,
                font=m_font,
                fill=(255, 255, 255),
            )
            current_h += u_height + pad
    if lower_text:
        for l_text in textwrap.wrap(lower_text, width=15):
            dim = draw.textbbox((0, 0), l_text, font=m_font)
            u_width, u_height = dim[2] - dim[0], dim[3] - dim[1]
            draw.text(
                xy=(
                    ((i_width - u_width) / 2) - 1,
                    i_height - u_height - int((20 / 640) * i_width),
                ),
                text=l_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(
                    ((i_width - u_width) / 2) + 1,
                    i_height - u_height - int((20 / 640) * i_width),
                ),
                text=l_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(
                    (i_width - u_width) / 2,
                    (i_height - u_height - int((20 / 640) * i_width)) - 1,
                ),
                text=l_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(
                    (i_width - u_width) / 2,
                    (i_height - u_height - int((20 / 640) * i_width)) + 1,
                ),
                text=l_text,
                font=m_font,
                fill=(0, 0, 0),
            )
            draw.text(
                xy=(
                    (i_width - u_width) / 2,
                    i_height - u_height - int((20 / 640) * i_width),
                ),
                text=l_text,
                font=m_font,
                fill=(255, 255, 255),
            )
            current_h += u_height + pad
    pics = check_filename("ultt.png")
    img.save(pics, "PNG")
    return pics
