# " Made by @e3ris for Ultroid "
# < https://github.com/TeamUltroid/Ultroid >
# This Plugin uses @Text2gifBot.

"""
✘ **Makes Fancy Gif from your Words!**

✘ `{i}t2g <some_text>`
    `Convert Text to Gif...`
"""

from os import remove
from random import randint

from emoji import replace_emoji

from . import ultroid_cmd, cleargif


@ultroid_cmd(pattern="t2g( (.*)|$)")
async def t2g(e):
    eris = await e.eor("`...`")
    input_args = e.pattern_match.group(2)
    if not input_args:
        input_args = "No Text was Given :(("
    args = replace_emoji(input_args, replace="")
    try:
        t2g = await e.client.inline_query("text2gifBot", args)
        doc = t2g[randint(0, len(t2g))]
        try:
            file = await doc.download_media()
            done = await e.client.send_file(
                e.chat_id, file=file, reply_to=e.reply_to_msg_id
            )
            remove(file)
        except AttributeError:
            # for files, without write Method
            done = await doc.click(e.chat_id, reply_to=e.reply_to_msg_id)
        await cleargif(done)
        await eris.delete()
    except Exception as fn:
        return await eris.edit(f"**ERROR** : `{fn}`")
