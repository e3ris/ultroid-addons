# Ultroid - UserBot
# Copyright (C) 2020 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

"""
✘ Commands Available -

• `{i}sticklet <text>`
   `create random sticker with text.`
"""

import asyncio
import io
import os
import random
import textwrap
from glob import glob

from PIL import Image, ImageDraw, ImageFont
from telethon.errors.rpcerrorlist import BotMethodInvalidError
from telethon.tl.types import InputMessagesFilterDocument

from . import *


@ultroid_cmd(pattern="sticklet( (.*)|$)")
async def sticklet(event):
    sticktext = event.pattern_match.group(2)
    if not sticktext:
        return await event.eor("`Give me some Text`")

    a = await event.eor(get_string("com_1"))
    R = random.randint(0, 256)
    G = random.randint(0, 256)
    B = random.randint(0, 256)
    sticktext = textwrap.wrap(sticktext, width=10)
    # converts back the list to a string
    sticktext = "\n".join(sticktext)
    image = Image.new("RGBA", (512, 512), (255, 255, 255, 0))
    draw = ImageDraw.Draw(image)
    fontsize = 230
    font_file_ = glob("resources/fonts/*ttf")
    FONT_FILE = random.choice(font_file_)
    font = ImageFont.truetype(FONT_FILE, size=fontsize)
    for i in range(10):
        dim = draw.multiline_textbbox((0, 0), sticktext, font=font)
        u_width, u_height = dim[2] - dim[0], dim[3] - dim[1]
        if not (u_width, u_height) > (512, 512):
            break
        fontsize = 100
        font = ImageFont.truetype(FONT_FILE, size=fontsize)
    dim = draw.multiline_textbbox((0, 0), sticktext, font=font)
    width, height = dim[2] - dim[0], dim[3] - dim[1]
    draw.multiline_text(
        ((512 - width) / 2, (512 - height) / 2), sticktext, font=font, fill=(R, G, B)
    )
    image_stream = io.BytesIO()
    image_stream.name = check_filename("ult.webp")
    image.save(image_stream, "WebP")
    image_stream.seek(0)
    await asyncio.gather(
        event.client.send_message(
            event.chat_id,
            sticktext,
            file=image_stream,
            reply_to=event.reply_to_msg_id,
        ),
        a.delete(),
        return_exceptions=True,
    )
