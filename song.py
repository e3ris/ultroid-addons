#    Ultroid - UserBot
#    Copyright 2020 (c)

# Lyrics ported from Dark Cobra
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

"""
✘ Commands Available -
• `{i}lyrics <search query>`
    Get lyrics of song.

• `{i}songs <search query>`
    alternative song command.
"""

import random
from io import BytesIO

from lyrics_extractor import SongLyrics
from lyrics_extractor.lyrics import LyricScraperException
from telethon.errors.rpcerrorlist import UserAlreadyParticipantError
from telethon.tl.functions.messages import ImportChatInviteRequest
from telethon.tl.types import InputMessagesFilterMusic as filtermus

from . import LOGS, ultroid_cmd


@ultroid_cmd(pattern="lyrics( (.*)|$)")
async def get_lyrics(event):
    if not (noob := event.pattern_match.group(2)):
        return await event.eor("`Give query to search lyrics for..`")

    ab = await event.eor("`Getting lyrics..`")
    api = random.choice(
        (
            "AIzaSyBF0zxLlYlPMp9xwMQqVKCQRq8DgdrLXsg",
            "AIzaSyDdOKnwnPwVIQ_lbH5sYE4FoXjAKIQV0DQ",
        )
    )
    extract_lyrics = SongLyrics(api, "15b9fb6193efd5d90")
    try:
        sh1vm = await extract_lyrics.get_lyrics(noob)
        assert type(sh1vm) == dict and bool(sh1vm.get("lyrics"))
    except (LyricScraperException, Exception):
        await ab.edit(f"`No Lyrics Found for {noob}..`")
        return LOGS.exception(f"Error in getting lyrics for {noob}!")

    title = sh1vm.get("title", noob)
    a7ul = f"**{title}** \n\n>__{sh1vm['lyrics']}__"
    if len(a7ul) > 4000:
        a7ul = f"> {title}\n\n\n> {sh1vm['lyrics']}"
        with BytesIO(a7ul.encode()) as file:
            file.name = f"{title} - lyrics.txt"
            await ab.respond(f"`{title}`", file=file)
            await ab.delete()
    else:
        await ab.edit(a7ul)


"""
@ultroid_cmd(pattern="song ?(.*)")
async def _(event):
    ultroid_bot = event.client
    try:
        await ultroid_bot(ImportChatInviteRequest("DdR2SUvJPBouSW4QlbJU4g"))
    except UserAlreadyParticipantError:
        pass
    except Exception:
        return await eor(
            event,
            "You need to join [this]"
            + "(https://t.me/joinchat/DdR2SUvJPBouSW4QlbJU4g)"
            + "group for this module to work.",
        )
    args = event.pattern_match.group(1)
    if not args:
        return await event.eor("`Enter song name`")
    okla = await event.eor("processing...")
    chat = -1001271479322
    current_chat = event.chat_id
    try:
        async for event in ultroid_bot.iter_messages(
            chat, search=args, limit=1, filter=filtermus
        ):
            await ultroid_bot.send_file(current_chat, event, caption=event.message)
        await okla.delete()
    except Exception:
        return await okla.eor("`Song not found.`")
"""
