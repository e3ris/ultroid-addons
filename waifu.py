# Ported Plugin

"""
✘ Commands Available -

• `{i}waifu <text>`
    paste text on random stickers.
"""

from random import choice

from emoji import replace_emoji

from . import ultroid_cmd, get_string


@ultroid_cmd(
    pattern="waifu( (.*)|$)",
)
async def waifu(animu):
    xx = await animu.eor(get_string("com_1"))
    # """Creates random anime sticker!"""
    text = animu.pattern_match.group(2)
    if not text:
        if animu.is_reply:
            text = (await animu.get_reply_message()).message
        else:
            await xx.edit(get_string("sts_1"))
            return
    waifus = (32, 33, 37, 40, 41, 42, 58, 20)
    finalcall = "#" + (str(choice(waifus)))
    text = replace_emoji(text, replace="")
    sticcers = await animu.client.inline_query("stickerizerbot", text)
    await sticcers[0].click(
        animu.chat_id,
        reply_to=animu.reply_to_msg_id,
        silent=bool(animu.is_reply),
        hide_via=True,
    )
    await xx.delete()
