# Ultroid - UserBot
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.

# Edited Plugin (for personal use)

"""
✘ Commands Available -

• `{i}autobio`
   `Start or Stop AUTOBIO.`
"""

from time import strftime

from telethon.tl.functions.account import UpdateProfileRequest

from . import async_searcher, LOGS, udB, ultroid_bot, scheduler


async def bio_updater():
    try:
        response = await async_searcher(
            "https://api.quotable.io/random?maxLength=62",
            re_json=True,
        )
        time = strftime("%H:%M")
        bio = f"{time} | {response.get('content')}"
        await ultroid_bot(UpdateProfileRequest(about=bio))
    except Exception:
        LOGS.warning("Error in AutoBio Updater", exc_info=True)


if udB.get_key("AUTOBIO") and scheduler:
    scheduler.add_job(
        bio_updater,
        trigger="interval",
        minutes=20,
        id="autobio",
        jitter=60,
    )


@ultroid_cmd(pattern="autobio$")
async def auto_bio(event):
    if udB.get_key("AUTOBIO"):
        udB.del_key("AUTOBIO")
        if scheduler:
            scheduler.remove_job("autobio")
        return await event.eor("`AUTOBIO has been Stopped !!`", time=10)

    msg = await event.eor("`Starting AUTOBIO...`")
    if scheduler:
        udB.set_key("AUTOBIO", True)
        scheduler.add_job(
            bio_updater,
            trigger="interval",
            minutes=20,
            id="autobio",
            jitter=60,
        )
    else:
        await msg.edit("`APScheduler is missing..`")
